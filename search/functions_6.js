var searchData=
[
  ['set_5fduty_94',['set_duty',['../classmotor_1_1MotorDriver.html#a1e2b28ede91f546709e0afccf7cfb15f',1,'motor::MotorDriver']]],
  ['set_5fkd_95',['set_Kd',['../classcontrollers_1_1PID.html#a12660021d53e9d3056202ba52b604455',1,'controllers::PID']]],
  ['set_5fki_96',['set_Ki',['../classcontrollers_1_1PID.html#a9f00059c920e31999490294e2cf8646c',1,'controllers::PID']]],
  ['set_5fkp_97',['set_Kp',['../classcontrollers_1_1Proportional.html#a797667fabebeb76f3fdb929561bb232d',1,'controllers.Proportional.set_Kp()'],['../classcontrollers_1_1PID.html#a1ba22deece47909ee7f9dd1c510d27f6',1,'controllers.PID.set_Kp()']]],
  ['set_5fmode_98',['set_mode',['../classimu_1_1Imu.html#ab176891a08d4035acad143d6e7a7c257',1,'imu::Imu']]],
  ['set_5fposition_99',['set_position',['../classencoder_1_1Encoder.html#a0c840a3ce4c5a9c9b7c24400ebb3aea6',1,'encoder::Encoder']]],
  ['set_5fset_5fpoint_100',['set_set_point',['../classcontrollers_1_1Proportional.html#aa7487d2e88013f379a667c88ebbb2a42',1,'controllers.Proportional.set_set_point()'],['../classcontrollers_1_1PID.html#a5607146f68437ef8adce8eddd4a22c48',1,'controllers.PID.set_set_point()']]]
];
