var face__detection__opencv_8py =
[
    [ "find_faces", "face__detection__opencv_8py.html#a032b6fc1ec49e3bcfc535e07de3d42ee", null ],
    [ "bus", "face__detection__opencv_8py.html#aae3c653e66e677594103d32c24199bae", null ],
    [ "camera", "face__detection__opencv_8py.html#a7a70d9860b4cac19213c76b0d46328de", null ],
    [ "exit", "face__detection__opencv_8py.html#a9567dc5aba40b5f4441fb2aca7fd03cf", null ],
    [ "face_cascade", "face__detection__opencv_8py.html#af876c5d212dbc65a93f91fe2431fdd7a", null ],
    [ "faces", "face__detection__opencv_8py.html#aab52032d1f40199a282d2b8fc399dfde", null ],
    [ "i2c_address", "face__detection__opencv_8py.html#a1b3195c72830d0a218ccab7d2a7b53ba", null ],
    [ "i2c_cmd", "face__detection__opencv_8py.html#a8432caef0f873c58559a91377a3537e1", null ],
    [ "rawCapture", "face__detection__opencv_8py.html#a00068f42f18cfe9b572e51f4c4e25e6c", null ],
    [ "resolution", "face__detection__opencv_8py.html#af3caaaf1244dd5e39bc855f92112f6a9", null ]
];