var lab7_8py =
[
    [ "addr", "lab7_8py.html#a1d8edcd82819372ab31d4f699790ba89", null ],
    [ "data", "lab7_8py.html#a5b58791d093e2fe476163e57572ac1b6", null ],
    [ "i2c", "lab7_8py.html#a14d6f33d0673b875d769e8f71f784879", null ],
    [ "pin2_EN", "lab7_8py.html#a28cf9efd33635a62ea28d9ab78bf1188", null ],
    [ "pin2_IN1", "lab7_8py.html#a3415de5f06250b74c85b3774ab30382f", null ],
    [ "pin2_IN2", "lab7_8py.html#af15ff6f56e3d2721d4ec6774eb5c460a", null ],
    [ "pin_EN", "lab7_8py.html#a5991b66293302e4eca35c51ae6dd6059", null ],
    [ "pin_IN1", "lab7_8py.html#a563f50541f5b7d4df33a431c6808fb3c", null ],
    [ "pin_IN2", "lab7_8py.html#acd1c6673ac6500d7a3d9a718e80eee6c", null ],
    [ "SLAVE", "lab7_8py.html#ab041e1a20ac812183a173291be37d543", null ],
    [ "tim", "lab7_8py.html#aa218baee9a27c51c83d57d37621cc623", null ],
    [ "tim2", "lab7_8py.html#ab1aad625b094aa5f055988e61bae5b08", null ],
    [ "x", "lab7_8py.html#a5be9b4f54c1ab9d3c0c3aeec7cd9eb28", null ],
    [ "x_driver", "lab7_8py.html#a5ccfd502c368d72ff7044432d9093031", null ],
    [ "y", "lab7_8py.html#a456fe7707d29fee0f015aa32ad6137d4", null ],
    [ "y_driver", "lab7_8py.html#aeb7c288a359bcf9b2222575392c81abe", null ]
];