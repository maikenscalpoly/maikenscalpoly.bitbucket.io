var project_8py =
[
    [ "addr", "project_8py.html#a0de69b7bd8b6b4f2dd09884817199816", null ],
    [ "data", "project_8py.html#a72c61b50e0797bb73fb63d397c6b5dba", null ],
    [ "i2c", "project_8py.html#a10304a2502a9853bbb267f17f302b467", null ],
    [ "pin2_EN", "project_8py.html#ab7ad9cc8088af3257249ce37dedd79f1", null ],
    [ "pin2_IN1", "project_8py.html#ab1aac0cff75cb378ccac9acec80c68a1", null ],
    [ "pin2_IN2", "project_8py.html#ae2f0441560c5a7f42b556b1a8a51000d", null ],
    [ "pin_EN", "project_8py.html#a8bce59fb6c508d749072475761a103f0", null ],
    [ "pin_IN1", "project_8py.html#ae68b179b86368b3091d57c465c7d123a", null ],
    [ "pin_IN2", "project_8py.html#a7467f9bb8dd6ce21e8c3dca409e456a1", null ],
    [ "SLAVE", "project_8py.html#a5a1fcf824d4857afc3407e47f848fb03", null ],
    [ "tim", "project_8py.html#ab0d1abceb25c02848944247b7f062b5c", null ],
    [ "tim2", "project_8py.html#adf9dc4e5377c5715939d63b5241f2efb", null ],
    [ "x", "project_8py.html#ad7ed6dae58a08d381ef034ad994d849a", null ],
    [ "x_driver", "project_8py.html#a146418ccdecb7c2a4ac21f53b1efb840", null ],
    [ "y", "project_8py.html#a8297aec382025748851d754cac61c1c0", null ],
    [ "y_driver", "project_8py.html#a3f373dca9ea02d854d9ad17aec9c000f", null ]
];