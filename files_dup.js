var files_dup =
[
    [ "controllers.py", "controllers_8py.html", [
      [ "Proportional", "classcontrollers_1_1Proportional.html", "classcontrollers_1_1Proportional" ],
      [ "PID", "classcontrollers_1_1PID.html", "classcontrollers_1_1PID" ]
    ] ],
    [ "encoder.py", "encoder_8py.html", "encoder_8py" ],
    [ "face_detection_opencv.py", "face__detection__opencv_8py.html", "face__detection__opencv_8py" ],
    [ "imu.py", "imu_8py.html", [
      [ "Imu", "classimu_1_1Imu.html", "classimu_1_1Imu" ]
    ] ],
    [ "lab0.py", "lab0_8py.html", "lab0_8py" ],
    [ "lab1.py", "lab1_8py.html", "lab1_8py" ],
    [ "lab3.py", "lab3_8py.html", "lab3_8py" ],
    [ "lab4.py", "lab4_8py.html", "lab4_8py" ],
    [ "motor.py", "motor_8py.html", "motor_8py" ],
    [ "project.py", "project_8py.html", "project_8py" ],
    [ "proposal.py", "proposal_8py.html", null ]
];