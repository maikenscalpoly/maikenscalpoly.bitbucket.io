var classmotor_1_1MotorDriver =
[
    [ "__init__", "classmotor_1_1MotorDriver.html#a7747ec05097d01fda608fcf3c205202a", null ],
    [ "disable", "classmotor_1_1MotorDriver.html#ab11aec882487b1adfccb64e0911211d4", null ],
    [ "enable", "classmotor_1_1MotorDriver.html#a505de7dca7e746db2c51d5ae14ad5823", null ],
    [ "set_duty", "classmotor_1_1MotorDriver.html#a1e2b28ede91f546709e0afccf7cfb15f", null ],
    [ "backward_channel", "classmotor_1_1MotorDriver.html#a95b402da9c7791040d7965682d55bc05", null ],
    [ "EN_pin", "classmotor_1_1MotorDriver.html#a4dbb99161c480e100e5dc5aa5b67ff84", null ],
    [ "forward_channel", "classmotor_1_1MotorDriver.html#af7843392e0cbb9ed40a7d70c5aac0611", null ],
    [ "IN1_pin", "classmotor_1_1MotorDriver.html#abd4faa96190a9532cd32f992190c1f64", null ],
    [ "IN2_pin", "classmotor_1_1MotorDriver.html#a694ac6e1a2bd20d58d024682a2cb31d9", null ],
    [ "timer", "classmotor_1_1MotorDriver.html#a41b9136956046016568e7d9f1c957e4f", null ]
];